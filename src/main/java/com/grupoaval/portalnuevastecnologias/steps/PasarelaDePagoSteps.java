package com.grupoaval.portalnuevastecnologias.steps;

import java.awt.AWTException;

import com.grupoaval.portalnuevastecnologias.pageobjects.PasarelaDePagoPage;

import net.thucydides.core.annotations.Step;

public class PasarelaDePagoSteps {
	PasarelaDePagoPage pagarProductos = new PasarelaDePagoPage();

	@Step
	public void clickEnProcederAlPago() throws InterruptedException {
		pagarProductos.botonProcederAlPago();

	}

	// fill form, because I need to pay my product
	@Step
	public void Nombre(String nombre) throws AWTException {
		pagarProductos.ingresarNombre(nombre);
	}

	@Step
	public void Apelido(String apellido) throws AWTException {
		pagarProductos.ingresarPrimerApelido(apellido);
	}

	@Step
	public void segundoApellido(String SegundoApellido) throws AWTException {
		pagarProductos.ingresarSegundoApellido(SegundoApellido);
	}

	@Step
	public void compania(String compania) {
		pagarProductos.ingresarCompania(compania);
	}

	@Step
	public void correo(String correo) {
		pagarProductos.ingresarCorreo(correo);
	}

	@Step
	public void telefono(String telefono) {
		pagarProductos.ingresarTelefono(telefono);
	}

	@Step
	public void pais(String pais) throws InterruptedException {
		pagarProductos.seleccionarPais(pais);
	}

	@Step
	public void ciudad(String ciudad) throws InterruptedException {
		pagarProductos.ingresarCiudad(ciudad);
	}

	@Step
	public void provincia(String provincia) throws InterruptedException {
		pagarProductos.ingresarProvincia(provincia);
	}

	@Step
	public void postal(String postal) throws InterruptedException {
		pagarProductos.ingresarPostal(postal);
	}

	@Step
	public void direccion(String direccion) throws InterruptedException {
		pagarProductos.ingresarDireccion(direccion);
	}

	@Step
	public void continuarPagando() throws InterruptedException {
		pagarProductos.botonContinuarPagando();
	}

	// Metodo de pago

	@Step
	public void confirmarPedido() throws InterruptedException {
		pagarProductos.botonConfirmarPedido();
	}

	@Step
	public void metodoPago() {
		pagarProductos.metodoDePago();
	}

	@Step
	public void banco(String banco) throws InterruptedException {
		pagarProductos.seleccionandoBanco(banco);
		Thread.sleep(1000);
	}

	@Step
	public void tipoPersona(String persona) throws InterruptedException {
		pagarProductos.tipoPersona(persona);
	}

	@Step
	public void titular(String titular) {
		pagarProductos.nombreTitular(titular);
	}

	@Step
	public void tipodocumento(String tipo) throws InterruptedException {
		pagarProductos.campoTipoDocumento(tipo);
	}

	@Step
	public void numero(String numero) {
		pagarProductos.numeroDocumento(numero);
	}

	@Step
	public void ingresarCorreo(String correo) {
		pagarProductos.correoElectronico(correo);
	}

	@Step
	public void ingresarTelefono(String telefono) {
		pagarProductos.telefono(telefono);
	}

	@Step
	public void pulsarBotonPagar() throws InterruptedException {
		pagarProductos.botonPago();
		pagarProductos.mensajeCompraExitosa();
	}
	
	@Step
	public void informacionPago() throws InterruptedException {
		pagarProductos.esperar();
	}
	

	@Step
	public void finalizarOrden() throws InterruptedException {
		pagarProductos.finalizarCompra();
	}
	
	@Step
	public void orden() {
		pagarProductos.mensajeCompraExitosa();
	}
	
	@Step
	public void enlacesutilesPago() {
		pagarProductos.enlacesUtilesPago();
	}
	
	
	@Step
	public void chulitoPago() {
		pagarProductos.chulitoDesplegable();
	}
	
	@Step
	public void espera() {
		pagarProductos.esperar();
	}
	
}